$(document).ready(function() {
    //call slick.js for client slider 
    $('.client-slider').slick({
        infinite: true,
        slidesToShow: 8,
        slidesToScroll: 2
    });
    //call clamp.js for trim paragrapth into 3 lines
    $('.product-shortintro').each(function(index, element) {
        $clamp(element, {clamp: 3, useNativeClamp: false});
    });

    //function for control sub menu in product list
    $('body').scrollspy({
        target: '.nav-sidebar',
        offset: 500
    });
    $(".nav-sidebar").affix({
        offset: {
            top: 100
        }
    });
//    $(".block-sidebar").affix({
//        offset: {
//            top: 520
//        }
//    });
});