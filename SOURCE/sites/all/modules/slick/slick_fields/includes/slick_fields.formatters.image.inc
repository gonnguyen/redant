<?php

/**
 * @file
 * Fields formatter for Slick and core Image field integration.
 */

/**
 * Format core image data.
 */
function _slick_fields_format_image(&$settings, $items, $field, $thumb_nav = FALSE) {
  $build = array();
  $css = array();

  $asnavfor = 'main';
  $slick_id = $settings['attributes']['id'];

  foreach ($items as $key => $item) {
    $slide = array(
      'item' => $item,
      'slide' => array(),
      'caption' => array(),
    );

    // Once media.module enabled, it populates type.
    $slide_type = isset($slide['item']['type']) ? $slide['item']['type'] : $field['type'];
    $settings['type'] = $slide_type;

    // Setup the variables.
    $image['uri']    = $slide['item']['uri'];
    $image['width']  = isset($slide['item']['width']) ? $slide['item']['width'] : NULL;
    $image['height'] = isset($slide['item']['height']) ? $slide['item']['height'] : NULL;
    $image['alt']    = $slide['item']['alt'];
    $image['title']  = $slide['item']['title'];

    $settings['media_height'] = $image['height'];
    $settings['media_width']  = $image['width'];

    $thumbnail = '';
    $thumbnail_hover = '';
    $image_slide = '';

    if (isset($settings['thumbnail_style']) && $settings['thumbnail_style']) {
      $image['path'] = $image['uri'];
      $image['style_name'] = $settings['thumbnail_style'];

      $thumbnail = theme('image_style', $image);

      if ($settings['thumbnail_hover']) {
        $thumbnail_hover = '<div class="slide__thumbnail--placeholder"><div class="slide__thumbnail">' . $thumbnail . '</div>';
        $thumbnail_hover .= '<button type="button" data-role="none">' . ($key + 1) . '</button></div>';
      }
    }

    if (!$thumb_nav) {
      // Slide captions.
      if (!empty($settings['slide_caption'])) {
        if (!empty($image['title'])) {
          $slide['caption']['title']['#markup'] = filter_xss($image['title']);
        }
        if (!empty($image['alt'])) {
          $slide['caption']['alt']['#markup'] = filter_xss($image['alt']);
        }
      }

      // Slide layouts.
      if (!empty($settings['slide_layout'])) {
        $slide['caption']['layout'] = check_plain($settings['slide_layout']);
      }

      // Image with picture and colorbox supports.
      $image_slide = _slick_fields_get_picture($settings, $image, $key);

      // Thumbnail static.
      if ($settings['dots_main'] && $thumbnail_hover) {
        $image_slide .= $thumbnail_hover;
      }
    }
    else {
      // Thumbnail slider if so configured.
      $image_slide = $thumbnail;
      $settings['type'] = 'image';
      $asnavfor = 'thumbnail';

      // Thumbnail static.
      if ($settings['dots_thumbnail'] && $thumbnail_hover) {
        $image_slide .= $thumbnail_hover;
      }
    }

    // Pass it over to theme_slick_media.
    $slide['slide'] = array(
      '#theme' => 'slick_media',
      '#item' => $image_slide,
      '#settings' => $settings,
      '#asnavfor' => $asnavfor,
    );

    // @todo map from picture, or refine for mobile.
    if (in_array($settings['skin'], array('fullscreen', 'parallax'))) {
      $slide['slide'] = array();
      $fullscreen_image_url = image_style_url($settings['image_style'], $image['uri']);
      $css[] = "#{$slick_id} .slide--{$key} {background-image: url('{$fullscreen_image_url}')}";
    }

    $build[$key] = $slide;
  }

  $settings['inline_css'] = $css;

  return $build;
}
