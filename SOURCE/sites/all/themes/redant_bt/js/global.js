jQuery(document).ready(function($) {
    //call clamp.js for trim paragrapth into 3 lines
    $('.product-shortintro p').each(function(index, element) {
        $clamp(element, {clamp: 3, useNativeClamp: false});
    });
    //function for control sub menu in product list
    $('body').scrollspy({
        target: '.nav-sidebar',
        offset: 500
    });
    $(".nav-sidebar").affix({
        offset: {
            top: 100
        }
    });

    window.$zopim || (function(d, s) {
        var z = $zopim = function(c) {
            z._.push(c)
        }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
        z.set = function(o) {
            z.set.
                    _.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0;
        $.setAttribute('charset', 'utf-8');
        $.src = '//v2.zopim.com/?2e8VN5PwUozihfTCg5cXnvGChlsrvZ5w';
        z.t = +new Date;
        $.
                type = 'text/javascript';
        e.parentNode.insertBefore($, e)
    })(document, 'script');
});