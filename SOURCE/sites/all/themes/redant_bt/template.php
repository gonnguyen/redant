<?php

/**
 * @file
 * template.php
 */


function redant_bt_preprocess_page(&$variables) {
  if (!empty($variables['node']) && !empty($variables['node']->type)) {
    if ($variables['node']->type == 'product')
      //dpm($variables['theme_hook_suggestions']);
      $variables['theme_hook_suggestions'][] = 'page__node__product';
      //dpm($variables['theme_hook_suggestions']);
      if (!isset($variables['breadcrumb'])) {
        global  $base_url;
        $variables['breadcrumb'] =
        "<div class='btn-group btn-breadcrumb'>
            <a href='".$base_url."' class='btn btn-default'>
              <i class='glyphicon glyphicon-home'></i>
            </a>
            <a href='".$base_url."/product-list' class='btn btn-default'>Sản phẩm</a>
            <a href='".$base_url."/".current_path()."' class='btn btn-default'>Shoppro P.Net</a>
        </div>";
      }
  }
}

function redant_bt_preprocess_node(&$variables) {

  // Get a list of all the regions for this theme
  if (isset($variables['type']) && $variables['type'] == 'product') {
    foreach (system_region_list($GLOBALS['theme']) as $region_key => $region_name) {

      // Get the content for each region and add it to the $region variable
      if ($blocks = block_get_blocks_by_region($region_key)) {
        $variables['region'][$region_key] = $blocks;
      }
      else {
        $variables['region'][$region_key] = array();
      }
    }
  }
}