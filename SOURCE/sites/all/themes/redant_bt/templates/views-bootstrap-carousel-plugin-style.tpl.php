<div id="views-bootstrap-carousel-<?php print $id ?>" class="block-slider <?php print $classes ?>" <?php print $attributes ?>>
  <!-- Carousel items -->
  <div class="carousel-inner">
    <?php foreach ($rows as $key => $row): ?>
      <div class="item <?php if ($key === 0) print 'active' ?>">
        <?php print $row ?>
      </div>
    <?php endforeach ?>
  </div>

  <?php if ($indicators): ?>
    <!-- Carousel indicators -->
    <?php global  $base_url;?>
    <ul class="carousel-indicators block-banner-control">
        <li class="active" data-target="#views-bootstrap-carousel-<?php print $id ?>" data-slide-to="0"><a href="#"><img src="<?php print  $base_url."/sites/all/themes/redant_bt/";?>images/icon-business.png"><span>Sales</span></a></li>
        <li data-target="#views-bootstrap-carousel-<?php print $id ?>" data-slide-to="1"><a href="#"><img src="<?php print  $base_url."/sites/all/themes/redant_bt/";?>images/icon-coffee.png"><span>Food/Drink</span></a></li>
        <li data-target="#views-bootstrap-carousel-<?php print $id ?>" data-slide-to="2"><a href="#"><img src="<?php print  $base_url."/sites/all/themes/redant_bt/";?>images/icon-phone.png"><span>Mobile</span></a></li>
        <li><a href="#"><span>Lĩnh vực <br> khác</span></a></li>
    </ul>
  <?php endif ?>

  <?php if ($navigation): ?>
    <!-- Carousel navigation -->
    <a class="carousel-control left" href="#views-bootstrap-carousel-<?php print $id ?>" data-slide="prev">
      <span class="icon-prev"></span>
    </a>
    <a class="carousel-control right" href="#views-bootstrap-carousel-<?php print $id ?>" data-slide="next">
      <span class="icon-next"></span>
    </a>
  <?php endif ?>
</div>
